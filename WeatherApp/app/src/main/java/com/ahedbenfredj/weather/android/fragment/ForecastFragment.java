package com.ahedbenfredj.weather.android.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ahedbenfredj.weather.android.R;
import com.ahedbenfredj.weather.android.adapter.ForecastAdapter;
import com.ahedbenfredj.weather.android.object.ForecastDay;

import java.util.ArrayList;

public class ForecastFragment extends ListFragment {

    ArrayList<ForecastDay> weekDays;
    private String lengthPreferences;
    private String temperaturePreferences;

    public ForecastFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forecast, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadPreferences();
        TextView aux = (TextView) getActivity().findViewById(R.id.my_action_bar_title);
        aux.setText(getText(R.string.fragment_forecast));
        setListAdapter(new ForecastAdapter(getActivity(), weekDays, temperaturePreferences));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void loadPreferences() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        lengthPreferences = preferences.getString("unitLength", "0");
        temperaturePreferences = preferences.getString("unitTemperature", "0");
    }

    public void setWeekDays(ArrayList<ForecastDay> weekDays) {
        this.weekDays = weekDays;
    }
}
