package com.ahedbenfredj.weather.android.activity;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.MenuItem;
import android.widget.TextView;
import com.ahedbenfredj.weather.android.R;

public class PreferencesActivity extends PreferenceActivity {


    public PreferencesActivity () {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        try {
            getActionBar().setDisplayShowCustomEnabled(true);
            getActionBar().setCustomView(R.layout.action_bar_title);
            getActionBar().setIcon(R.drawable.ab_solid_example);
            TextView aux = (TextView) findViewById(R.id.my_action_bar_title);
            aux.setText(getText(R.string.action_settings));
        } catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return false;
    }
}
