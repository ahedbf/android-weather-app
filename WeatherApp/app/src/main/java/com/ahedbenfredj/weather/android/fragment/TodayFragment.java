package com.ahedbenfredj.weather.android.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.ahedbenfredj.weather.android.R;
import com.ahedbenfredj.weather.android.activity.MainActivity;
import com.ahedbenfredj.weather.android.object.ForecastDay;


public class TodayFragment extends Fragment {

    private TextView tvLocation, tvTemperature, tvWeatherdesc, tvHumidity, tvPrecipitation, tvPressure, tvWindSpeed, tvDirection;
    private ImageView ivWeatherIcon;
    private ForecastDay today;
    private String lengthPreferences;
    private String temperaturePreferences;
    /*private Button btnRestart;*/

    public TodayFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_today, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadViews();
        loadPreferences();
        getActivity().getActionBar().setTitle(R.string.fragment_today);
        if (MainActivity.isLocated()) {
           if (today != null) {
                rederViews();
            }
        } else {
        }
    }

    public void loadViews() {
        ivWeatherIcon = (ImageView) getView().findViewById(R.id.ivWeatherToday);
        tvLocation = (TextView) getView().findViewById(R.id.tvLocationToday);
        tvTemperature = (TextView) getView().findViewById(R.id.tvTemperatireToday);
        tvWeatherdesc = (TextView) getView().findViewById(R.id.tvWeatherDesc);
        tvHumidity = (TextView) getView().findViewById(R.id.tvHumidityToday);
        tvPrecipitation = (TextView) getView().findViewById(R.id.tvPrecipitationToday);
        tvPressure = (TextView) getView().findViewById(R.id.tvPressureToday);
        tvWindSpeed = (TextView) getView().findViewById(R.id.tvWindSpeedToday);
        tvDirection = (TextView) getView().findViewById(R.id.tvDirectionToday);
    }

    private void loadPreferences() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        lengthPreferences = preferences.getString("unitLength", "0");
        temperaturePreferences = preferences.getString("unitTemperature", "0");
    }

    private void rederViews() {
        ivWeatherIcon.setImageBitmap(today.getWeatherIcon());
        tvLocation.setText(today.getLocation());
        if (temperaturePreferences.equals("0")) tvTemperature.setText(today.getTempC() + " ");
        else if (temperaturePreferences.equals("1")) tvTemperature.setText(today.getTempF() + " ");
        tvWeatherdesc.setText(" " + today.getWeatherDesc());
        tvHumidity.setText(today.getHumidity());
        tvPrecipitation.setText(today.getPrecipitation());
        tvPressure.setText(today.getPressure());
        if (lengthPreferences.equals("0")) tvWindSpeed.setText(today.getWindSpeedKmh());
        else if (lengthPreferences.equals("1")) tvWindSpeed.setText(today.getWindSpeedMph());
        tvDirection.setText(today.getDirection());
    }

    public void setToday (ForecastDay today) {
        this.today = today;
    }
}
