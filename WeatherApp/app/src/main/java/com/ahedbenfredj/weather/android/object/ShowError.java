package com.ahedbenfredj.weather.android.object;

import android.content.Context;
import android.widget.Toast;

public class ShowError {
    public static void ShowError(Context context,String error) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show();
    }
}
