package com.ahedbenfredj.weather.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ahedbenfredj.weather.android.R;
import com.ahedbenfredj.weather.android.object.ForecastDay;
import java.util.ArrayList;

public class ForecastAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ForecastDay> forecastWeek;
    private String temperaturePreferences;

    public ForecastAdapter(Context context, ArrayList<ForecastDay> forecastWeek, String temperaturePreferences) {
        this.context = context;
        this.forecastWeek = forecastWeek;
        this.temperaturePreferences = temperaturePreferences;
    }

    @Override
    public int getCount() {
        return forecastWeek.size();
    }

    @Override
    public Object getItem(int position) {
        return forecastWeek.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        LayoutInflater inflater = LayoutInflater.from(context);
        if(convertView==null)
            view = inflater.inflate(R.layout.forecast_list_item, null,
                    true);
        ImageView ivWeatherIcon = (ImageView) view.findViewById(R.id.ivForecastIcon);
        TextView tvForecastDay = (TextView) view.findViewById(R.id.tvForecastDay);
        TextView tvForecastTemperature = (TextView) view.findViewById(R.id.tvForecastTemperature);
        ivWeatherIcon.setImageBitmap(forecastWeek.get(position).getWeatherIcon());
        tvForecastDay.setText(forecastWeek.get(position).getWeatherDesc()+" on "+forecastWeek.get(position).getWeekDay());
        if (temperaturePreferences.equals("1")) tvForecastTemperature.setText(forecastWeek.get(position).getTempF());
        else tvForecastTemperature.setText(forecastWeek.get(position).getTempC());
        return view;
    }
}
