package com.ahedbenfredj.weather.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ahedbenfredj.weather.android.R;
import com.ahedbenfredj.weather.android.object.NavegationDrawerItem;

import java.util.ArrayList;

public class DrawerAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NavegationDrawerItem> navDrawerItems;

    public DrawerAdapter(Context context, ArrayList<NavegationDrawerItem> navDrawerItems) {
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = LayoutInflater.from(context);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }
        ImageView ivIcon = (ImageView) convertView.findViewById(R.id.ivIconDrawer);
        TextView tvTittle = (TextView) convertView.findViewById(R.id.tvDrawerText);
        ivIcon.setImageResource(navDrawerItems.get(position).getIcon());
        tvTittle.setText(navDrawerItems.get(position).getTittle());
        return convertView;
    }
}
