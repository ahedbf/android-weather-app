package com.ahedbenfredj.weather.android.service;

import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class JSONWeatherForecastRequest {

    public JSONObject getDataJSON(String url_server) {
        InputStream inputStream = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url_server));
            inputStream = httpResponse.getEntity().getContent();
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return parseJSONString(inputStream);
    }

    private JSONObject parseJSONString (InputStream inputStream) {
        String result = "";
        if (inputStream!=null){
            try{
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                inputStream.close();
                result = sb.toString();
            }catch(Exception e){
                return  null;
            }
            try{
                JSONObject jArray = new JSONObject(result);
                return jArray;
            }
            catch(JSONException e){
                return null;
            }
        }
        else{
            return null;
        }
    }
}

